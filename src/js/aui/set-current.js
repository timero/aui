'use strict';

import $ from './jquery';
import { fn as deprecateFn } from './internal/deprecation';
import globalize from './internal/globalize';

/**
 * Shortcut function adds or removes 'current' classname to an element based on a passed boolean.
 *
 * @param {String | Element} element The element or an ID to show or hide.
 * @param {boolean} show True to add 'current' class, false to remove.
 *
 * @returns {undefined}
 */
function setCurrent (element, current) {
    if (!(element = $(element))) {
        return;
    }

    if (current) {
        element.addClass('current');
    } else {
        element.removeClass('current');
    }
}

var setCurrent = deprecateFn(setCurrent, 'setCurrent', {
    sinceVersion: '5.9.0',
    extraInfo: 'No alternative will be provided. Use jQuery.addClass() / removeClass() instead.'
});

globalize('setCurrent', setCurrent);

export default setCurrent;
