'use strict';

import $ from './jquery';
import * as deprecate from './internal/deprecation';
import * as logger from './internal/log';
import globalize from './internal/globalize';

/**
 * Support for markup based binder components. Binder components must be objects with the following "interface":
 *
 * <pre>
 * {
 *   selector: "input.foo",
 *   run: function(element) {
 *      //do stuff on given element
 *   }
 * }
 * </pre>
 */
var Binder = (function () {
    'use strict';

    var binders = {};

    return {
        /**
         * Runs all the binder components for the given scope, or the document body if none specified.
         *
         * @method runBinders
         * @param scope {Element} element scope to run the binders in
         */
        runBinders: function (scope) {
            if ($.isEmptyObject(binders)) {
                logger.log('No binders to run');
                return;
            }

            scope = scope || document.body;

            $('*:not(link, script)', scope).each(function (i, element) {
                var $element = $(element);
                $.each(binders, function (id, binder) {
                    if (!$element.data(id) && $element.is(binder.selector)) {
                        logger.log('Running binder component: ' + id + ' on element ' + element);
                        $element.data(id, true); // so we don't bind to the same element again later
                        binder.run(element);
                    }
                });
            });

        },

        /**
         * Register a binder component with the given id.
         * @method register
         */
        register: function (id, binder) {
            binders[id] = binder;
        },

        /**
         * Unregister a binder component for the given id.
         * @method unregister
         */
        unregister: function (id) {
            binders[id] = null;
        }
    };
}());

Binder = deprecate.construct(Binder, 'Binder', {
    sinceVersion: '5.8.0'
});

globalize('Binder', Binder);

export default Binder;
