'use strict';

import $ from '../jquery';
import binder from '../binder';

(function () {
    // browser supports placeholder, no need to do anything
    var temp = document.createElement('input');

    if ('placeholder' in temp) {
        return;
    }

    /**
     * Displays default text in the input field when its value is empty.
     * If the browser supports placeholder input attributes (HTML5), then
     * we skip this component.
     *
     * Usage:
     * <pre>
     * &lt;input placeholder='Some default text'&gt;
     * </pre>
     *
     * Events thrown: reset.placeholder
     */
    binder.register('placeholder', {
        selector: 'input[placeholder]',
        run: function (element) {
            var $this = $(element);
            var applyDefaultText = function () {
                if (!$.trim($this.val()).length) {
                    $this.val($this.attr('placeholder'))
                        .addClass('placeholder-shown')
                        .trigger('reset.placeholder');
                }
            };

            applyDefaultText();
            $this.blur(applyDefaultText).focus(function () {
                if ($this.hasClass('placeholder-shown')) {
                    $this.val('').removeClass('placeholder-shown');
                }
            });
        }
    });
}());
