(function () {
    /* eslint-disable no-undef */
    var $ = window.jQuery;
    var _ = window._;
    var auiMessages = AJS.messages;
    var RestfulTable = AJS.RestfulTable;
    var contextPath = AJS.contextPath();
    /* eslint-enable no-undef */

    var EditGroupView = RestfulTable.CustomEditView.extend({
        render: function (self) {
            var $select = $("<select name='group' class='select'>" +
                    "<option value='Friends'>Friends</option>" +
                    "<option value='Family'>Family</option>" +
                    "<option value='Work'>Work</option>" +
                    "</select>");

            $select.val(self.value); // select currently selected
            return $select;
        }
    });

    var NameReadView = RestfulTable.CustomReadView.extend({
        render: function (self) {
            return $("<strong />").text(self.value);
        }
    });

    var CheckboxEditView = RestfulTable.CustomEditView.extend({
        render: function (self) {
            var $select = $("<input type='checkbox' class='ajs-restfultable-input-" + self.name + "' />" +
                "<input type='hidden' name='" + self.name + "'/>");
            return $select;
        }
    });

    var DummyReadView = RestfulTable.CustomReadView.extend({
        render: function () {
            return $("<strong />").text("Blah");
        }
    });

    // DOM ready
    $(function () {
        var auiEvents = ["ROW_ADDED", "REORDER_SUCCESS", "ROW_REMOVED", "EDIT_ROW"];
        _.each(auiEvents, function(eventName){
            $(document).one(RestfulTable.Events[eventName], function(){
                auiMessages.info("#message-area", {
                    id: eventName,
                    title: "test",
                    body: eventName + " fired on AJS. Used for testing AJS events."
                });
            });
        });

        var url = contextPath + "/rest/contacts/1.0/contacts";
        var $contactsTable = $("#contacts-table");
        var $contactsAddPositionBottomTable = $("#contacts-table-addPositionBottom");

        new RestfulTable({
            el: $contactsTable, // <table>
            autofocus: true, // auto focus first field of create row
            columns: [
                {id: "name", header: "Name", readView: NameReadView}, // id is the mapping of the rest property to render
                {id: "group", header: "Group", editView: EditGroupView}, // header is the text in the <th>
                {id: "number", header: "Number"},
                {id: "checkbox", header: "Checkbox", readView: DummyReadView, editView: CheckboxEditView}
            ],
            resources: {
                all: url, // resource to get all contacts
                self: url // resource to get single contact url/{id}
            },
            noEntriesMsg: "You have no contacts. Loner!", // message to be displayed when table is empty
            allowReorder: true, // drag and drop reordering
            fieldFocusSelector: function(name) {
                return ":input[type!=hidden][name=" + name + "], #" + name + ", .ajs-restfultable-input-" + name;
            }
        });

        // duplicate of the first table but with the addPosition: "bottom" option applied.
        new RestfulTable({
            el: $contactsAddPositionBottomTable, // <table>
            autofocus: true, // auto focus first field of create row
            columns: [
                {id: "name", header: "Name", readView: NameReadView}, // id is the mapping of the rest property to render
                {id: "group", header: "Group", editView: EditGroupView}, // header is the text in the <th>
                {id: "number", header: "Number"},
                {id: "checkbox", header: "Checkbox", readView: DummyReadView, editView: CheckboxEditView}
            ],
            resources: {
                all: url, // resource to get all contacts
                self: url // resource to get single contact url/{id}
            },
            noEntriesMsg: "You have no contacts. Loner!", // message to be displayed when table is empty
            allowReorder: true, // drag and drop reordering
            fieldFocusSelector: function(name) {
                return ":input[type!=hidden][name=" + name + "], #" + name + ", .ajs-restfultable-input-" + name;
            },
            addPosition: "bottom"
        });
    });

}());
