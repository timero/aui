/* global Clipboard, AJS, $, document */
$(function () {
    if (!document.getElementById('icons-list')) {
        return;
    }

    var iconDimmedClassName = 'icon-list-dimmed';

    function renderIconList(iconNames, container) {
        container.innerHTML = '';

        iconNames.forEach(function(icon) {
            var iconSpan = document.createElement('span');
            iconSpan.className = 'aui-icon aui-icon-small aui-iconfont-' + icon;
            iconSpan.textContent = 'Insert meaningful text here for accessibility';

            var iconSpanApiHtml = iconSpan.outerHTML;

            iconSpan.setAttribute('data-clipboard-text', iconSpanApiHtml);
            iconSpan.title = icon;
            container.appendChild(iconSpan);

            $(iconSpan).tooltip();
        });
    }

    $.ajax('../assets/icons-list.json').then(function(icons) {
        renderIconList(icons, document.getElementById('icons-list'));

        var allIconElements = Array.prototype.slice.call(document.getElementById('icons-list').querySelectorAll('.aui-icon'));

        function iconSearchHandler (e) {
            var query = e.target.value.toLowerCase();

            allIconElements.forEach(function(icon) {
                var iconName = icon.getAttribute('original-title');
                var shouldHighlightIcon = (query === '' || iconName.indexOf(query) !== -1);

                if (shouldHighlightIcon) {
                    icon.classList.remove(iconDimmedClassName);
                } else {
                    icon.classList.add(iconDimmedClassName);
                }
            });
        }

        document.getElementById('search-icons').addEventListener('input', iconSearchHandler);

        var clipboard = new Clipboard('[data-clipboard-text]');

        clipboard.on('success', function (e) {
            AJS.flag({
                type: 'success',
                title: 'Copied to clipboard.',
                body: e.text + ' has been copied to the clipboard.',
                close: 'auto'
            });
        });

        clipboard.on('error', function () {
            AJS.flag({
                type: 'warning',
                title: 'Icon copying failed to load.',
                body: 'There was a problem copying the icon to the clipboard',
                close: 'auto'
            });
        });
    });
});



