import getNpmPackageInfo from './npm-package-info';

const auiPackageName = '@atlassian/aui';

const STABLE_REG_EXP = /^[0-9]+\.[0-9]\.+[0-9]+$/;
const MINOR_REG_EXP = /^[0-9]+\.[0-9]+/;

const matchStableVersion = version => version.match(STABLE_REG_EXP);
const getMinorVersionPart = version => version.match(MINOR_REG_EXP).pop();

export function getAllVersions() {
    return getNpmPackageInfo(auiPackageName).then(result => {
        const {versions} = result;

        return Object.keys(versions);
    });
}

export function getStableVersions(versions) {
    return versions
        .filter(matchStableVersion)
        .sort((a, b) => {
            let [majorA, minorA, patchA] = a.split('.');
            let [majorB, minorB, patchB] = b.split('.');

            if (majorA !== majorB) {
                return majorA - majorB;
            }

            if (minorA !== minorB) {
                return minorA - minorB;
            }

            return patchA - patchB;
        });
}

export function getLatestStableVersions(versions) {
    return versions.filter((version, i) => {
        const nextVersion = versions[i + 1];

        if (nextVersion) {
            return getMinorVersionPart(version) !== getMinorVersionPart(nextVersion);
        }

        return true;
    });
}
