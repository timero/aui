var gulpCssnano = require('gulp-cssnano');

module.exports = function () {
  return gulpCssnano({mergeLonghand: false}); // Necessary until https://github.com/ben-eb/cssnano/issues/116 is fixed
};
