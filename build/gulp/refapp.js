const gat = require('gulp-auto-task');
const gulp = require('gulp');

module.exports = gulp.series(
    gat.load('plugin/build'),
    gat.load('refapp/run')
);
