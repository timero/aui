const request = require('request');
const { name: auiPkgName } = require('../../../package.json');

const STABLE_REG_EXP = /^[0-9]+\.[0-9]\.+[0-9]+$/;
const MINOR_REG_EXP = /^[0-9]+\.[0-9]+/;

const matchStableVersion = version => version.match(STABLE_REG_EXP);
const getMinorVersionPart = version => version.match(MINOR_REG_EXP).pop();

function getStableVersions(versions) {
    return versions
        .filter(matchStableVersion)
        .sort((a, b) => {
            let [majorA, minorA, patchA] = a.split('.');
            let [majorB, minorB, patchB] = b.split('.');

            if (majorA !== majorB) {
                return majorA - majorB;
            }

            if (minorA !== minorB) {
                return minorA - minorB;
            }

            return patchA - patchB;
        });
}

function getLatestStableVersions(versions) {
    return versions.filter((version, i) => {
        const nextVersion = versions[i + 1];

        if (nextVersion) {
            return getMinorVersionPart(version) !== getMinorVersionPart(nextVersion);
        }

        return true;
    });
}

module.exports = function libGetAuiVersions() {
    return new Promise((resolve, reject) => {
        request(`https://registry.npmjs.com/${auiPkgName.replace('/', '%2F')}`, { json: true }, (err, resp, json) => {
            if (err) {
                reject(err);
            }

            const allVersions = Object.keys(json.versions);
            const stableVersions = getStableVersions(allVersions);
            const latestVersions = getLatestStableVersions(stableVersions);

            let versions = latestVersions.reverse(); // Newest versions on top

            resolve(versions);
        });
    });
};
