var gulp = require('gulp');
var gulpDebug = require('gulp-debug');

module.exports = function distLicense () {
    return gulp.src('LICENSE.md')
        .pipe(gulpDebug({title: 'license'}))
        .pipe(gulp.dest('dist'));
};
