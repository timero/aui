'use strict';

var gat = require('gulp-auto-task');
var mavenStdoutFilter = require('../../tasks/lib/maven-stdout-filter');
var path = require('path');
var spawn = require('child_process').spawn;

var opts = gat.opts();

module.exports = function pluginMavenInstall (done) {
    var INTEGRATION_PATH = 'integration';
    var mavenArguments = [
        'clean',
        'install',
        '-DskipTests',
        '-DskipAllPrompts=true',
        `-Djquery.version=${opts.jquery || '1.8.3'}`,
        `-Daui.location=${path.resolve('.')}`,
    ];

    mavenArguments.push(['-X']);

    console.log(`Executing mvn ${mavenArguments.join(' ')}`);

    var cmd = spawn('mvn', mavenArguments, {
        cwd: INTEGRATION_PATH
    });

    cmd.stdout.on('data', mavenStdoutFilter(opts.verbose));
    cmd.stderr.on('data', mavenStdoutFilter(opts.verbose));

    cmd.on('close', function(code) {
        done(code);
    });
};
