'use strict';

var path = require('path');
var gulp = require('gulp');
var gulpReplace = require('gulp-replace');
var resolvePackagePath = require('../../lib/resolve-package-path');

function replaceAtlaskitSharedStyleImports (relativepath) {
    return gulpReplace('~@atlaskit/util-shared-styles/src', function() {
        let filepath = this.file.path;
        let relativeish = path.relative(filepath, relativepath);
        return `${relativeish}/atlaskit`;
    });
}

function copySharedAtlaskitStyles () {
    return gulp
        .src('node_modules/@atlaskit/util-shared-styles/src/**/*')
        .pipe(gulp.dest('.tmp/plugin/src/less/imports/atlaskit'));
}

function copyAuiLess () {
    return gulp
        .src([
            'src/less/**/*',
            '!src/less/imports/**',
            '!src/less/batch/**'
        ])
        .pipe(gulp.dest('.tmp/plugin/src/less'));
}

function copyAuiVendorCss () {
    return gulp
        .src('src/css-vendor/**/*')
        .pipe(gulp.dest('.tmp/plugin/src/css-vendor'));
}

function copyAuiLessTheme () {
    return gulp
        .src('src/less/imports/aui-theme/**/*')
        .pipe(replaceAtlaskitSharedStyleImports('src/less/imports/aui-theme'))
        .pipe(gulp.dest('.tmp/plugin/src/less/imports/aui-theme'));
}

function copyFancyFileInputCss() {
    return gulp
        .src(resolvePackagePath('fancy-file-input', 'dist/fancy-file-input.css'))
        .pipe(gulp.dest('.tmp/plugin/node_modules/fancy-file-input/dist'));
}

function cherryPickGlobalImports () {
    return gulp
        .src([
            'src/less/imports/global.less',
            'src/less/imports/mixins.less',
        ])
        .pipe(gulp.dest('.tmp/plugin/src/less/imports'));
}

function copyGlobalMixins () {
    return gulp
        .src('src/less/imports/mixins/**/*.less')
        .pipe(gulp.dest('.tmp/plugin/src/less/imports/mixins'));
}

// Note that we don't currently compile the LESS for AUI at build-time.
// Rather, we're copying it over in to a plugin, and the LESS is compiled at product runtime.
// It's kinda bad, and we should change that approach.
module.exports = gulp.series(
    copySharedAtlaskitStyles,
    copyAuiLess,
    copyAuiVendorCss,
    copyAuiLessTheme,
    copyFancyFileInputCss,
    cherryPickGlobalImports,
    copyGlobalMixins
);
