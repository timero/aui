var webpack = require('webpack');

function buildWithWebpack (config) {
    return function buildingWithWebpack (cb) {
        webpack(config, (err, stats) => {
            if (err) {
                throw new Error('webpack', err);
            }
            console.log(stats.toString({
                chunks: false,
                colors: true
            }));
            cb();
        });
    }
}

module.exports = {
    buildWithWebpack
};
