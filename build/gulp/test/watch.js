'use strict';

var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var karma = require('../../lib/karma');
var rootPaths = require('../../lib/root-paths');

var taskBuildTest = gat.load('test/build');
var opts = gat.opts({
    watch: true
});

module.exports = gulp.series(
    taskBuildTest,
    function watchTests (done) {
        var srcPaths = rootPaths('src/**');
        var testPaths = rootPaths('tests/**');
        gulp.watch(srcPaths.concat(testPaths), taskBuildTest).on('change', galv.cache.expire);
        done();
    },
    function watch (done) {
        karma(opts, done).start();
    }
);
