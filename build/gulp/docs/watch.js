var docsOpts = require('../../lib/docs-opts');
var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpWebserver = require('gulp-webserver');
var path = require('path');

var opts = gat.opts();
var { host, port, path: serverPath } = Object.assign(docsOpts, opts);

var taskDocs = gat.load('docs/webpack');

module.exports = gulp.series(
    taskDocs,
    function docsWatch (done) {
        gulp.watch([
            path.join(opts.root, 'docs/**'),
            path.join(opts.root, 'src/**'),
            'docs/**',
            'src/**'
        ], taskDocs).on('change', galv.cache.expire);
        done();
    },
    function docsWatchServe () {
        return gulp.src('.tmp/docs')
            .pipe(gulpWebserver({
                host,
                port,
                path: serverPath,
                livereload: false,
                open: serverPath,
            }));
    }
);
