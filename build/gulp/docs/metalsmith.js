'use strict';

const docsOpts = require('../../lib/docs-opts');
const gulpOpts = require('gulp-auto-task').opts();
const del = require('del');
const metalsmith = require('metalsmith');
const metalsmithLayouts = require('metalsmith-layouts');
const metalsmithInPlace = require('metalsmith-in-place');
const metalsmithMarkdown = require('metalsmith-markdown');
const metalsmithDefine = require('metalsmith-define');
const metalsmithRootpath = require('metalsmith-rootpath');
const metalsmithDiscoverHelpers = require('metalsmith-discover-helpers');
const metalsmithDiscoverPartials = require('metalsmith-discover-partials');

const libGetAuiVersions = require('../lib/get-aui-versions');
const pkg = require('../../../package.json');
const adg3colourMap = require('../../../docs/src/assets/adg2-to-adg3-colours.json');

module.exports = function docsMetalsmith (done) {
    libGetAuiVersions().then(versions => {
        const opts = Object.assign({}, docsOpts, gulpOpts);
        const version = opts.docsVersion || pkg.version;

        const localDistUri = `//${opts.host}:${opts.port}/${opts.path}`;
        const cdnDistUri = `//unpkg.com/${pkg.name}@${version}/dist/aui`;

        const distLocation = opts.localdist ? localDistUri : cdnDistUri;

        const ms = metalsmith('./docs')
            .destination('../.tmp/docs') // Relative to the working directory
            .use([
                metalsmithRootpath(),

                metalsmithDefine({
                    version,
                    versions,
                    distLocation,
                    adg3colourMap
                }),

                metalsmithMarkdown(),

                metalsmithDiscoverHelpers({
                    directory: 'helpers',
                    pattern: /\.js$/,
                }),

                metalsmithDiscoverPartials({
                    directory: 'partials',
                    pattern: /\.hbs$/,
                }),

                // Protip: If compilations bail because of errors like 'cannot find partial',
                // it may be because multiple versions of handlebars are being loaded, thus the partials
                // are not propagating from the "discover" plugins over to this plugin.
                // Ensure that there is only one handlebars version throughout this whole repo!
                metalsmithInPlace({
                    pattern: '**/*.hbs'
                }),

                // Note: At this point, the docs files have a .html extension.

                metalsmithLayouts({
                    directory: 'layouts',
                    engine: 'handlebars',
                    default: 'main-layout.hbs',
                    pattern: '**/*.html'
                }),
            ]);

        ms.build(function (err) {
            if (err) {
                throw err;
            }

            del([
                '.tmp/docs/entry',
                '.tmp/docs/layouts',
                '.tmp/docs/src',
                '.tmp/docs/js',
                '.tmp/docs/scripts',
                '.tmp/docs/styles'
            ]).then(() => done());
        });
    });
};
