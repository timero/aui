'use strict';

var gat = require('gulp-auto-task');
var gulp = require('gulp');
var path = require('path');

var opts = gat.opts();

function src () {
    var args = [].slice.call(arguments);
    return gulp.src(args.map(function (glob) {
        return path.join(opts.root, glob);
    }).concat(args));
}

module.exports = gulp.series(
    function auiDist () {
        return src('dist/aui/**')
            .pipe(gulp.dest('.tmp/flatapp/src/static/common'));
    },
    function auiWebpackedDist () {
        // this'll override the stuff built via the gulp process.
        return src('dist/webpacked/**')
            .pipe(gulp.dest('.tmp/flatapp/src/static/common'));
    },
    function flatappSoy () {
        return src('tests/flatapp/src/soy/**/*.soy')
            .pipe(gulp.dest('.tmp/flatapp/src/soy'));
    },
    function flatappStatic () {
        return src('tests/flatapp/src/static/**')
            .pipe(gulp.dest('.tmp/flatapp/src/static'));
    },
    function testPagesSoy () {
        return src('tests/test-pages/pages/**/*.soy')
            .pipe(gulp.dest('.tmp/flatapp/src/soy/pages'));
    },
    function testPagesNotSoy () {
        return src('tests/test-pages/pages/**', '!**/*.soy')
            .pipe(gulp.dest('.tmp/flatapp/src/static/pages'));
    },
    function testPagesCommonSoy () {
        return src('tests/test-pages/common/**/*.soy')
            .pipe(gulp.dest('.tmp/flatapp/src/soy/dependencies'));
    },
    function testPagesCommonCss () {
        return src('tests/test-pages/common/**.css', 'tests/test-pages/common/**.js')
            .pipe(gulp.dest('.tmp/flatapp/src/static/common'));
    }
);
