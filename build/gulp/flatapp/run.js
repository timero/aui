var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpWebserver = require('gulp-webserver');

let opts = gat.opts();
let { host, port } = Object.assign({
    host: '0.0.0.0',
    port: 7000
}, opts);

module.exports = gulp.series(
    gat.load('flatapp/build'),
    function flatappServer () {
        return gulp.src('.tmp/flatapp/target/static')
            .pipe(gulpWebserver({
                host,
                port,
                livereload: false,
                open: 'pages',
            }));
    }
);
