import format from '../../../src/js/aui/format';

describe('aui/format', function () {
    it('globals', function () {
        expect(AJS.format).to.equal(format);
    });

    it('with 1 parameter', function () {
        var testFormat = format('hello {0}', 'world');
        expect(testFormat).to.equal('hello world');
    });

    it('with 2 parameters', function () {
        var testFormat = format('hello {0} {1}', 'world', 'again');
        expect(testFormat).to.equal('hello world again');
    });

    it('with 3 parameters', function () {
        var testFormat = format('hello {0} {1} {2}', 'world', 'again', '!');
        expect(testFormat).to.equal('hello world again !');
    });

    it('with 4 parameters', function () {
        var testFormat = format('hello {0} {1} {2} {3}', 'world', 'again', '!', 'test');
        expect(testFormat).to.equal('hello world again ! test');
    });

    it('with symbols', function () {
        var testFormat = format('hello {0}', '!@#$%^&*()');
        expect(testFormat).to.equal('hello !@#$%^&*()');
    });

    it('with curly braces', function () {
        var testFormat = format('hello {0}', '{}');
        expect(testFormat).to.equal('hello {}');
    });

    it('with repeated parameters', function () {
        var testFormat = format('hello {0}, {0}, {0}', 'world');
        expect(testFormat).to.equal('hello world, world, world');
    });

    it('with apostrophe', function () {
        var testFormat = format('hello \'{0}\' {0} {0}', 'world');
        expect(testFormat).to.equal('hello {0} world world');
    });

    it('with very long parameters', function () {
        var testFormat = format('hello {0}', new Array(25).join('this parameter is very long ')); // we're joining 25 empty values together, which means we'll get our join string 24 times.
        expect(testFormat).to.equal('hello this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long this parameter is very long '); // eslint-disable-line
    });

    // choices
    it('with a choice value missing parameter', function () {
        var testFormat = format('We got {0,choice,0#|1#1 issue|1<{1,number} issues}');
        expect(testFormat).to.equal('We got ');
    });

    it('with a choice value with parameter lower first option', function () {
        var testFormat = format('We got {0,choice,0#0 issues|1#1|1<{1,number} issues}', -1, -1);
        expect(testFormat).to.equal('We got 0 issues');
    });

    it('with a choice value first option', function () {
        var testFormat = format('We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}', 0);
        expect(testFormat).to.equal('We got 0 issues');
    });

    it('with a choice value middle option', function () {
        var testFormat = format('We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}', 1);
        expect(testFormat).to.equal('We got 1 issue');
    });

    it('with a choice value last option', function () {
        var testFormat = format('We got {0,choice,0#0 issues|1#1 issue|1<{0,number} issues}', 2);
        expect(testFormat).to.equal('We got 2 issues');
    });

    it('with a choice value with missing number parameter option', function () {
        var testFormat = format('We got {0,choice,0# |1#1 issue|1<{1,number} issues}', 2);
        expect(testFormat).to.equal('We got  issues');
    });

    it('with a choice value with valid second option', function () {
        var testFormat = format('We got {0,choice,0# |1#1 issue|1<{1,number} issues}', 10, 10);
        expect(testFormat).to.equal('We got 10 issues');
    });

    // number
    it('with a number value', function () {
        var testFormat = format('Give me {0,number}!', 5);
        expect(testFormat).to.equal('Give me 5!');
    });

    it('with a number value', function () {
        var testFormat = format('Give me {0,number}!');
        expect(testFormat).to.equal('Give me !');
    });
});
